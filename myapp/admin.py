from django.contrib import admin
from .models import student, course, prof, attn
# Register your models here.
class studentadmin(admin.ModelAdmin):
    list=['useri','name','roll','year','email','dep']
admin.site.register(student,studentadmin)
class courseadmin(admin.ModelAdmin):
    list=['name','subject']
admin.site.register(course,courseadmin)
class profadmin(admin.ModelAdmin):
    list=['useri','name','course','photo']
admin.site.register(prof,profadmin)
class attnadmin(admin.ModelAdmin):
    list=['student','course','ATT']
admin.site.register(attn,attnadmin)


